import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileLauncherComponent } from './auth/profile-launcher/profile-launcher.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGaurd } from './auth/auth.guard'
import { PlayerComponent } from './player/player.component';
import { PlayerhGaurd } from './player/player.gaurd';

const routes: Routes = [

	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full'
	},
	{
		path: 'login',
		component: ProfileLauncherComponent
	},
	{
		path: 'player/:id',
		component: PlayerComponent,
		canActivate: [AuthGaurd, PlayerhGaurd]
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		canActivate: [AuthGaurd],
	},
	{
		path: '**',
		component: DashboardComponent
	}

];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	providers: [AuthGaurd, PlayerhGaurd]
})
export class AppRoutingModule { }
