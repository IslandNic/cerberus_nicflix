import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store'
import * as fromRoot from '../app.reducer'
import * as Auth from '../auth/auth.actions';
import { VgAPI } from 'videogular2/core';

@Component({
	selector: 'app-player',
	templateUrl: './player.component.html',
	styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
	movieId: number;
	isGuest$: Observable<boolean>;

	constructor(private router: Router,
		private store: Store<fromRoot.State>,
		private activatedroute: ActivatedRoute,
		private videoApi: VgAPI) { }

	ngOnInit() {
		this.movieId = parseInt(this.activatedroute.snapshot.params.id);
		this.isGuest$ = this.store.select(fromRoot.getIsGuest);
	}

	onPlayerReady(api: VgAPI) {
		this.videoApi = api;

		//Add to started store when playing
		this.videoApi.getDefaultMedia().subscriptions.playing.subscribe(
			() => {
				this.store.dispatch(new Auth.AddToStarted({ movieId: this.movieId}));
			}
		)

		// Set the video to the beginning when completed
		// set completed in state
		this.videoApi.getDefaultMedia().subscriptions.ended.subscribe(
			() => {
				this.videoApi.getDefaultMedia().currentTime = 0;
				this.store.dispatch(new Auth.AddToCompleted({ movieId: this.movieId}));
			}
		);
	}






	back() {
		this.router.navigateByUrl('dashboard');
	}

	search() {
		this.router.navigateByUrl('dashboard');
	}


	addToWishlist() {
		this.store.dispatch(new Auth.AddToWishList({ movieId: this.movieId }));
	}

	addFavs() {
		this.store.dispatch(new Auth.AddToWFavourite({ movieId: this.movieId }));
	}

	addtoContinueWatching() {

	}
}
