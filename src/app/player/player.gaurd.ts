import { Injectable } from "@angular/core";
import { CanActivateChild, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router/";
import { Observable, of } from "rxjs";
import { Store, select } from "@ngrx/store";
import * as fromRoot from '../app.reducer';

import { tap, flatMap, filter, map } from "rxjs/operators";
import { DashboardDataService } from '../dashboard/dashboard-data.service';

@Injectable()
export class PlayerhGaurd implements CanActivate {
    
    constructor(private store: Store<fromRoot.State>, private router: Router, private dataService: DashboardDataService) {

    }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)  : Observable<boolean> {
        return this.dataService.movieKeyExist(parseInt(state.url.split("/")[2]))
        .pipe(
            tap(keyFound => {
                if(!keyFound) {
                    this.router.navigateByUrl('/dashboard');
                }
            })
        )
    }
}