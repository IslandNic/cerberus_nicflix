import * as fromUI from './shared/ui.reducer';
import * as fromAuth from './auth/auth.reducers';
import * as fromDashboard from './dashboard/dashboard.reducers'
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';


export interface State {
    ui: fromUI.UiState,
    auth: fromAuth.AuthState,
    dashboard: fromDashboard.DashboardState,
}


export const reducers: ActionReducerMap<State> = {
    ui: fromUI.uiReducer,
    auth: fromAuth.authReducer,
    dashboard: fromDashboard.dashboardReducer
}

export const getUiState = createFeatureSelector<fromUI.UiState>('ui');
export const getIsLoading = createSelector(getUiState, fromUI.getIsLoading);
export const getIsSearchOpen = createSelector(getUiState, fromUI.getIsSearchOpen);

export const getAuthState = createFeatureSelector<fromAuth.AuthState>('auth');
export const getIsLoggedin = createSelector(getAuthState, fromAuth.getIsLoggedIn);
export const getUser = createSelector(getAuthState, fromAuth.getUser);
export const getSpotlight = createSelector(getAuthState, fromAuth.getSpotlight);
export const getIsGuest  = createSelector(getAuthState, fromAuth.getIsGuest);

export const getDashboardState = createFeatureSelector<fromDashboard.DashboardState>('dashboard');
export const getAllMovies = createSelector(getDashboardState, fromDashboard.getAllMovies);
export const getRecentlyAdded = createSelector(getDashboardState, fromDashboard.getRecentlyAdded);
export const getTrendingNow = createSelector(getDashboardState, fromDashboard.getTrendingNow);