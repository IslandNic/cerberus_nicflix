import { UIActions, START_LOADING, STOP_LOADING, SEARCH_OPEN } from './ui.actions'


export interface UiState {
    isLoading: boolean;
    searchOpen: boolean;
  
}
const initialState: UiState = {
    isLoading: false,
    searchOpen: false
}

export function uiReducer(state = initialState, action: UIActions ) {

    switch (action.type) {
        case START_LOADING:
            return {
                isLoading: true,
                searchOpen: state.searchOpen
            };
            case STOP_LOADING:
                return {
                    isLoading: false,
                    searchOpen: state.searchOpen
                };
                case SEARCH_OPEN:
                    return {
                        searchOpen: action.payload.isOpen,
                        isLoading: state.isLoading,
        
                    };
        default:
            return state;
    }
}

export const getIsLoading = (state: UiState) => state.isLoading;
export const getIsSearchOpen = (state: UiState) => state.searchOpen;