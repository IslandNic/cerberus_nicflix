import { Action } from '@ngrx/store';

export const START_LOADING = "[UI] Start Loading"
export const STOP_LOADING = "[UI] Stop Loading"
export const SEARCH_OPEN = "[UI] Search Open"

export class StartLoading implements Action {
    readonly type = START_LOADING;
}
export class StopLoading implements Action {
    readonly type = STOP_LOADING;
}
export class SearchOpen implements Action {
    readonly type = SEARCH_OPEN;
    constructor (public payload: {isOpen: boolean}) {
    }
}

export type UIActions = StartLoading | StopLoading | SearchOpen; 