import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../models/movie.interface';
import { map, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'


@Component({
	selector: 'app-scroller',
	templateUrl: './scroller.component.html',
	styleUrls: ['./scroller.component.scss']
})
export class ScrollerComponent implements OnInit {

	@Input() movieList$: Observable<Array<Movie>>;
	@Input() feedType: string;
	themes =[];


	constructor(
	) { }

	ngOnInit() {
	
	}
}
