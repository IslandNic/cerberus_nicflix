import { Directive, Input, EventEmitter, HostListener, Renderer, Inject, ElementRef } from '@angular/core';

@Directive({ selector: '[appScroll]' })
export class Scroller {
    @Input('direction') direction: string;
	constructor(private el: ElementRef) {}

	@HostListener('click', ['$event'])
	onClick(event: MouseEvent) {
		this.el.nativeElement.scrollRight + 150;
	}
}