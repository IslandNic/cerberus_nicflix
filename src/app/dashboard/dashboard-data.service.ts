import { Movie } from './models/movie.interface';

import { Injectable } from '@angular/core';
import { movieData } from '../server-data/movie.mock-data';
import { platformStats } from '../server-data/platform-stats-data';
import { Observable, merge, combineLatest } from "rxjs";
import { of } from "rxjs";
import { PlatformStats } from '../dashboard/models/platform-data.interface';
import { filter, flatMap, mergeMap, map, tap, catchError, toArray } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app.reducer';
import * as Dashboard from '../dashboard/dashboard.actions';
import * as Auth from '../auth/auth.actions';


@Injectable({
    providedIn: 'root'
})
export class DashboardDataService {
    started = [];
    completed = [];

    constructor(private store: Store<fromRoot.State>, ) { }

    //mock server calls

    getPlatformStats(): Observable<PlatformStats> {
        return of(platformStats);
    }

    getMovieData() {
        return this.store.dispatch(new Dashboard.GetAllMovies({ allMovies: movieData }));
    }

    getAllMovies(): Observable<Array<Movie>> {
        return this.store.select(fromRoot.getAllMovies);
    }

    getbyGenre(val:string){
        return this.store.select(fromRoot.getAllMovies).pipe(
            map(item => item.filter(movie => movie.genres.includes(val)))
        )
    }

    getRecentlyAdded() {
        this.store.select(fromRoot.getAllMovies)
            .pipe(
                map(res => res.filter(item => platformStats.recentlyAdded.includes(item.id))),
                tap(items => {
                    this.store.dispatch(new Dashboard.GetRecentlyAdded({ recentlyAdded: items }))
                })
            ).subscribe();
    }

    getTrendingNow() {
        this.store.select(fromRoot.getAllMovies)
            .pipe(
                map(res => res.filter(item => platformStats.trendingNow.includes(item.id))),
                tap(items => {
                    this.store.dispatch(new Dashboard.GetTrendingNow({ trendingNow: items }));
                })
            ).subscribe();
    }

    movieKeyExist(key: number): Observable<boolean> {
        let verified: boolean;

        this.store.select(fromRoot.getAllMovies)
            .pipe(
                flatMap(data => data),
                filter(res => res.id === key),
                map(movie => {
                    movie ? verified = true : verified = false;
                })
            ).subscribe();

        return of(verified);
    }

    getUserProperty(prop: any) {
        let res = [];
        this.store.select(fromRoot.getUser)
            .pipe(

                map(item => item[prop]),
                catchError(err => of([])),
                tap(items => {
                    res = items;
                })
            ).subscribe();
        return res;
    }

    processMovieProperty(prop: any) {
        return this.store.select(fromRoot.getAllMovies)
            .pipe(
                map(res => res.filter(item => prop.includes(item.id))),
                tap(items => {
                    return items;
                })
            );
    }

    getUserStartedMovies(): Observable<Array<Movie>> {

        return this.processMovieProperty(this.getUserProperty('startedMovies'));

    }

    getUserCompletedMovies(): Observable<Array<Movie>> {

        return this.processMovieProperty(this.getUserProperty('completedMovies'));

    }

    getFavourites(): Observable<Array<Movie>> {

        return this.processMovieProperty(this.getUserProperty('favs'));

    }

    getWishlist(): Observable<Array<Movie>> {

        return this.processMovieProperty(this.getUserProperty('wishList'));

    }

    setRandomSpotlight() {
        let generatedId = Math.floor(Math.random() * 23)
        this.store.select(fromRoot.getAllMovies)
            .pipe(
                map(res => res.filter(item => item.id === generatedId)),
                tap(item => {
                    this.store.dispatch(new Auth.SetSpotlight({ movie: item[0] }))
                })).subscribe();
    }
}
