import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { ScrollerComponent } from './scroller/scroller.component';
import { DashboardComponent } from './dashboard.component';
import { SpotlightComponent } from './spotlight/spotlight.component';
import { SeachComponent } from './seach/seach.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng4-click-outside';
import { Scroller } from '../dashboard/scroller/scroll.directive';
import { ValuesPipe } from '../shared/values.pipe'


@NgModule({
    declarations: [MovieCardComponent, ScrollerComponent, DashboardComponent, SpotlightComponent, SeachComponent, Scroller, ValuesPipe],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AngularFontAwesomeModule,
        BrowserModule,
        AppRoutingModule,
        ClickOutsideModule,
     
    ],
    exports: [
        Scroller
    ],
    providers: [
   
    ],
    bootstrap: []

})
export class DashboardModule { }
