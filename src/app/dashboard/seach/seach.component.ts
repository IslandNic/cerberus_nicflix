import { Component, OnInit } from '@angular/core';
import * as fromRoot from '../../app.reducer';
import * as UI from '../../shared/ui.actions';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, noop, Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { debounceTime, distinctUntilChanged, tap, map } from 'rxjs/operators';
import { DashboardDataService } from '../dashboard-data.service';
import { Movie } from '../models/movie.interface';
import * as Auth from '../../auth/auth.actions';

@Component({
    selector: 'app-seach',
    templateUrl: './seach.component.html',
    styleUrls: ['./seach.component.scss']
})
export class SeachComponent implements OnInit {
    form: FormGroup;
    subscription: Subscription;
    searchResult$: Array<Movie>;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private store: Store<fromRoot.State>,
        private datatService: DashboardDataService
    ) { }

    ngOnInit() {
        this.buildForm();
        this.form.controls['searchField'].valueChanges.pipe(
            debounceTime(600),
            distinctUntilChanged(),
        ).subscribe((input) => {

            if (input !== "") {
                this.datatService.getAllMovies().pipe(
                    map(res => res.filter(item => item.key.includes(input.toLowerCase()))),
                    tap(items => {
                        items ? this.searchResult$ = items : null;
                    })
                ).subscribe();
             }
            
        })
    }


    selectSearchItem(val: Movie){
        this.store.dispatch(new Auth.SetSpotlight({movie: val}));
        this.form.controls['searchField'].setValue("");
        this.searchResult$ = null;
    }

    buildForm() {
        this.form = this.formBuilder.group({
            searchField: ['',],
        });
    }

    search() {
        this.store.dispatch(new UI.SearchOpen({ isOpen: true }));
    }

    outsideInputClick(){
        this.form.controls['searchField'].setValue("");
    }

}
