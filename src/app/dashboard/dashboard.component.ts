import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import { Store } from '@ngrx/store'
import * as fromRoot from '../app.reducer'
import { Observable } from 'rxjs';
import * as Auth from '../auth/auth.actions';
import { map, tap } from 'rxjs/operators'
import { Movie } from './models/movie.interface';
import { DashboardDataService } from './dashboard-data.service';
import { Router } from '@angular/router';
import * as UI from '../shared/ui.actions';
import { genreType, GenreType } from './models/movie.model';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    @ViewChild('searchResults') searchResults: ElementRef

    movieList$: Observable<Array<Movie>>;
    recentlyAdded$: Observable<Array<Movie>>;
    trendingNow$: Observable<Array<Movie>>;
    startedMovies$: Observable<Array<Movie>>;
    completedMovies$: Observable<Array<Movie>>;
    favouriteMovies$: Observable<Array<Movie>>;
    wishList$: Observable<Array<Movie>>;
    searchOpen$: Observable<boolean>;
    searchResults$: Observable<Array<Movie>>;
    title: string;
    isGuest$: Observable<boolean>;
    genreTypes = genreType;
    genreVisible = false;
    

    constructor(
        private dataService: DashboardDataService,
        private store: Store<fromRoot.State>,
        private router: Router
    ) { }

    ngOnInit() {
        this.dataService.getMovieData();
        this.dataService.getTrendingNow();
        this.dataService.getRecentlyAdded();
        
        this.recentlyAdded$ = this.store.select(fromRoot.getRecentlyAdded);
        this.trendingNow$ = this.store.select(fromRoot.getTrendingNow);
        this.searchOpen$ = this.store.select(fromRoot.getIsSearchOpen);
        this.startedMovies$ = this.dataService.getUserStartedMovies();
        this.completedMovies$ = this.dataService.getUserCompletedMovies();
        this.favouriteMovies$ = this.dataService.getFavourites();
        this.movieList$ = this.store.select(fromRoot.getAllMovies);
        this.wishList$ = this.dataService.getWishlist();
        this.isGuest$ = this.store.select(fromRoot.getIsGuest);

    }

    logout(){
        this.store.dispatch(new Auth.LogoutUser());
    }
    close(){
        this.store.dispatch(new UI.SearchOpen({isOpen: false}));
    }

    showWishlist(){
        this.searchResults$ = this.dataService.getWishlist();
        this.store.dispatch(new UI.SearchOpen({isOpen: true}));
        this.title = "Your Wishlist";
    }

    showGenre(){
        this.genreVisible = !this.genreVisible;
        
    }

    findGenre(value: string){
        this.genreVisible = false;
        this.searchResults$ = this.dataService.getbyGenre(value)
        this.store.dispatch(new UI.SearchOpen({isOpen: true}));
        this.title = `${value} movies`;
        this.searchResults.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    }

    showFavourites(){
        this.searchResults$ = this.dataService.getFavourites();
        this.store.dispatch(new UI.SearchOpen({isOpen: true}));
        this.title = "Your Favourites";
    }

    onClickedOutside($event){
        this.store.dispatch(new UI.SearchOpen({isOpen: false}));
    }

}
