import { Action } from '@ngrx/store';
import { Movie } from './models/movie.interface'

export const GET_ALL_MOVIES = "[Dasboard] Get All Movies"
export const GET_SPOTLIGHT = "[Dashboard] Get Movie by ID"
export const GET_RECENTLY_ADDED = "[Dashboard] Get Recently Added"
export const GET_TRENDING_NOW = "[Dashboard] Get Trending Now"


export class GetAllMovies implements Action {
    readonly type = GET_ALL_MOVIES;
    constructor (public payload: {allMovies: Array<Movie>}) {
    }
}
export class GetRecentlyAdded implements Action {
    readonly type = GET_RECENTLY_ADDED;
    constructor (public payload: {recentlyAdded: Array<Movie>}) {
    }
}
export class GetTrendingNow implements Action {
    readonly type = GET_TRENDING_NOW;
    constructor (public payload: {trendingNow: Array<Movie>}) {
    }
}

export type DashboardActions = GetAllMovies | GetRecentlyAdded | GetTrendingNow; 