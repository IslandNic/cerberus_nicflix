import { Component, OnInit } from '@angular/core';
import { DashboardDataService } from './../dashboard-data.service';
import { Observable } from 'rxjs';
import { Movie } from '../models/movie.interface'
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store'
import * as fromRoot from '../../app.reducer'
import * as Auth from '../../auth/auth.actions';

@Component({
  selector: 'app-spotlight',
  templateUrl: './spotlight.component.html',
  styleUrls: ['./spotlight.component.scss']
})
export class SpotlightComponent implements OnInit {
  spotlightItem: Movie;
  isGuest$: Observable<boolean>

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.store.select(fromRoot.getSpotlight).pipe(
      tap(movie => {
        this.spotlightItem = movie;
      })
    ).subscribe();
    this.isGuest$ = this.store.select(fromRoot.getIsGuest);
  }

  addToWishlist(){
    this.store.dispatch(new Auth.AddToWishList({movieId: this.spotlightItem.id}));
  }

  addToFavs(){
    this.store.dispatch(new Auth.AddToWFavourite({movieId: this.spotlightItem.id}));
  }
}
