import { DashboardActions, GET_ALL_MOVIES, GET_RECENTLY_ADDED, GET_TRENDING_NOW } from './dashboard.actions'

import { Movie } from './models/movie.interface';

export interface DashboardState {
    allMovies: Array<Movie>;
    recentlyAddedMovies: Array<Movie>
    trendingNow: Array<Movie>
}
const initialDashboardState: DashboardState = {
    allMovies: [],
    recentlyAddedMovies: [],
    trendingNow: []
}

export function dashboardReducer(state = initialDashboardState, action: DashboardActions) {

    switch (action.type) {
        case GET_ALL_MOVIES:
            return {
                allMovies: action.payload.allMovies,
                recentlyAddedMovies: state.recentlyAddedMovies,
                trendingNow: state.trendingNow
            };

        case GET_RECENTLY_ADDED:
            return {
                recentlyAddedMovies: action.payload.recentlyAdded,
                allMovies: state.allMovies,
                trendingNow: state.trendingNow
            };

        case GET_TRENDING_NOW:
            return {
                trendingNow: action.payload.trendingNow,
                allMovies: state.allMovies,
                recentlyAddedMovies: state.recentlyAddedMovies
            };

        default:
            return state;
    }
}

export const getAllMovies = (state: DashboardState) => state.allMovies;
export const getRecentlyAdded = (state: DashboardState) => state.recentlyAddedMovies;
export const getTrendingNow = (state: DashboardState) => state.trendingNow;
