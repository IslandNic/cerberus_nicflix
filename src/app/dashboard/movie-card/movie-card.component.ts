import { Component, OnInit, Input, ViewChild, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { Movie } from '../models/movie.interface';
import * as Auth from '../../auth/auth.actions';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';


@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {


  @Input() movie: Movie
  
  constructor( private store: Store<fromRoot.State>,
    private renderer: Renderer2

    ) { }

  ngOnInit() {
  }


  setSpotlight(){
    this.store.dispatch(new Auth.SetSpotlight({movie: this.movie}))
  }
}
