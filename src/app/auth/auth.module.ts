import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileLauncherComponent } from './profile-launcher/profile-launcher.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule, Actions } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store'
import * as fromAuth from '../auth/auth.reducers'
import { AuthEffects } from './auth.effects';

@NgModule({
  declarations: [
    ProfileLauncherComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    StoreModule.forFeature('auth', fromAuth.authReducer),
    EffectsModule.forFeature([AuthEffects])
  ]
})
export class AuthModule { }
