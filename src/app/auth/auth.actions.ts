import { Action } from '@ngrx/store';
import { User } from './user.model'
import { Movie } from '../dashboard/models/movie.interface';

export const LOGIN_USER = "[Auth] Login User";
export const LOGOUT_USER = "[Auth] Logout User";
export const SET_SPOTLIGHT = "[Auth] Set Spotlight";
export const ADD_TO_WISHLIST = "[Auth User] Add to Wishlist";
export const ADD_TO_FAVOURITE = "[Auth User] Add to Favourite";
export const ADD_TO_STARTED = "[Auth User] Add to Started";
export const ADD_TO_COMPLETED = "[Auth User] Add to Completed";
export const IS_GUEST = "[Auth User] Is guest"

export class LoginUser implements Action {
    readonly type = LOGIN_USER;
    constructor (public payload: {user: User}) {
    }
}
export class LogoutUser implements Action {
    readonly type = LOGOUT_USER;
}
export class SetSpotlight implements Action {
    readonly type = SET_SPOTLIGHT;
    constructor (public payload: {movie: Movie}) {
    }
}
export class AddToWishList implements Action {
    readonly type = ADD_TO_WISHLIST;
    constructor (public payload: {movieId: number}) {
    }
}
export class AddToWFavourite implements Action {
    readonly type = ADD_TO_FAVOURITE;
    constructor (public payload: {movieId: number}) {
    }
}
export class AddToStarted implements Action {
    readonly type = ADD_TO_STARTED;
    constructor (public payload: {movieId: number}) {
    }
}

export class AddToCompleted implements Action {
    readonly type = ADD_TO_COMPLETED;
    constructor (public payload: {movieId: number}) {
    }
}
export class IsGuest implements Action {
        readonly type = IS_GUEST;
        constructor (public payload: {isGuest: boolean}) {
        }
    }

export type AuthActions = LoginUser | LogoutUser | SetSpotlight | AddToWishList | AddToWFavourite | AddToStarted | AddToCompleted | IsGuest;