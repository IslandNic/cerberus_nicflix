import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, defer, of } from 'rxjs';
import { ofType } from '@ngrx/effects'
import * as fromAuth from '../auth/auth.reducers'
import { LoginUser, LOGIN_USER, LogoutUser, LOGOUT_USER } from '../auth/auth.actions'
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as Auth from '../auth/auth.actions';
import * as fromRoot from '../app.reducer';
import { Store } from '@ngrx/store'

@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private store: Store<fromRoot.State>,
    ) { }

    @Effect({ dispatch: false }) login$ = this.actions$.pipe(
        ofType<LoginUser>(LOGIN_USER),
        tap(action => {
            localStorage.setItem("user", JSON.stringify(action.payload.user));
        }
        ));


    @Effect({ dispatch: false }) spotlight$ = this.actions$.pipe(
        ofType<Auth.SetSpotlight>(Auth.SET_SPOTLIGHT),
        tap(action => {
            localStorage.setItem("spotlight", JSON.stringify(action.payload.movie));
        }
        ));

    @Effect({ dispatch: false }) addToWishlist$ = this.actions$.pipe(
        ofType<Auth.AddToWishList | Auth.AddToWFavourite>(Auth.ADD_TO_WISHLIST),
        tap(() => {
            this.store.select(fromRoot.getUser).pipe(
                tap((item) => {
                    localStorage.setItem("user", JSON.stringify(item));
                })
            ).subscribe()
        }
        ));

    @Effect({ dispatch: false }) addToFavourite$ = this.actions$.pipe(
        ofType<Auth.AddToWFavourite>(Auth.ADD_TO_FAVOURITE),
        tap(() => {
            this.store.select(fromRoot.getUser).pipe(
                tap((item) => {
                    localStorage.setItem("user", JSON.stringify(item));
                })
            ).subscribe()
        }
        ));

    @Effect({ dispatch: false }) logout$ = this.actions$.pipe(
        ofType<LogoutUser>(LOGOUT_USER),
        tap(action => {
            localStorage.removeItem("user");
            localStorage.removeItem("spotlight");
            this.router.navigateByUrl('/login');

        }
        ));


    @Effect()
    init$ = defer(() => {

        const userData = localStorage.getItem("user");
        const spotlight = localStorage.getItem("spotlight");

        

        if (userData) {
            let isGuest: Boolean;

            let user = JSON.parse(localStorage.getItem("user"));
            user.name === "Guest" ? isGuest = true : null;

            if (!isGuest) {
                this.store.dispatch(new Auth.SetSpotlight({ movie: JSON.parse(spotlight) }));
                this.store.dispatch(new Auth.LoginUser({ user: JSON.parse(userData) })); 
            }
        } else {
            this.store.dispatch(new Auth.LogoutUser());
        }
    })
}