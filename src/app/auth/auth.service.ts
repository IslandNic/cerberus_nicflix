
import { User } from './user.model';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app.reducer';
import * as UI from '../shared/ui.actions';
import * as Auth from '../auth/auth.actions';
import { Router } from '@angular/router';
import { DashboardDataService } from '../dashboard/dashboard-data.service'
import { Movie } from '../dashboard/models/movie.interface';


@Injectable({
    providedIn: 'root'
})
export class AuthService {
    user: User;

    constructor(
        private store: Store<fromRoot.State>,
        private router: Router,
        private dataService: DashboardDataService
    ) { }


    // mock backend authentication with profile
    loginUser(name: string) {
        this.store.dispatch(new UI.StartLoading());
        
        switch (name.toString().toLowerCase()) {
            case 'nicholis':

                this.user = {
                    name: "Nicholis",
                    email: "nicholis.muller@gmail.com",
                    favs: [1, 5, 8],
                    startedMovies: [9, 12, 21, 18],
                    completedMovies: [3, 5, 9, 21, 16, 13, 17],
                    wishList: [10, 19],
                    focussed: Math.floor(Math.random() * 23)
                }


                //mock server delay and login
                setTimeout(() => {
                    this.store.dispatch(new Auth.LoginUser({user: this.user}));
                    this.router.navigateByUrl('/dashboard');
                    this.dataService.setRandomSpotlight();
                    // this.store.dispatch(new Auth.IsGuest({isGuest: false}));
                    this.store.dispatch(new UI.StopLoading());
                }, 1000);

                return true;

            case 'dewi':
                this.user = {
                    name: "Dewi",
                    email: "donsie78@gmail.com",
                    favs: [3, 4, 9, 4],
                    startedMovies: [2, 4, 6, 12, 15],
                    completedMovies: [21, 4, 8, 19, 11, 8],
                    wishList: [],
                    focussed: Math.floor(Math.random() * 23)
                }

                //mock server delay and login
                setTimeout(() => {
                    this.store.dispatch(new Auth.LoginUser({user: this.user}));
                    this.router.navigateByUrl('/dashboard');
                    this.dataService.setRandomSpotlight();
                    // this.store.dispatch(new Auth.IsGuest({isGuest: false}));
                    this.store.dispatch(new UI.StopLoading());
                }, 1000);

                return true;

            case 'guest':
                this.user = {
                    name: "Guest",
                    email: "",
                    favs: [],
                    startedMovies: [],
                    completedMovies: [],
                    wishList: [],
                    focussed: Math.floor(Math.random() * 23)
                }

                //mock server delay and login
                setTimeout(() => {
                    this.store.dispatch(new Auth.LoginUser({user: this.user}));
                    this.store.dispatch(new Auth.IsGuest({isGuest: true}));
                    this.router.navigateByUrl('/dashboard');
                    this.dataService.setRandomSpotlight();
                    this.store.dispatch(new UI.StopLoading());
                }, 2000);

                return;

            default:
                setTimeout(() => {
                    this.store.dispatch(new UI.StopLoading());
                }, 2000);
                return false;
        }
    }
}
