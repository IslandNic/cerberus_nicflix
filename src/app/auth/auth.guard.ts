import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router/";
import { Observable } from "rxjs";
import { Store, select } from "@ngrx/store";
import * as fromRoot from '../app.reducer';

import { tap } from "rxjs/operators";

@Injectable()
export class AuthGaurd implements CanActivate {
    
    constructor(private store: Store<fromRoot.State>, private router: Router) {

    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> {

        return this.store.select(fromRoot.getIsLoggedin)
        .pipe(
            tap((loggedIn) => {
                if(!loggedIn) {
                    this.router.navigateByUrl('/login');
                }
            })
        )
    }
}