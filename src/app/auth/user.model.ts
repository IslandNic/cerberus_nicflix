export interface User {
    name : string,
    email : string,
    favs : Array<number>,
    startedMovies : Array<number>,
    completedMovies : Array<number>,
    wishList : Array<number>,
    focussed: number
}