import { AuthActions, LOGIN_USER, LOGOUT_USER, SET_SPOTLIGHT, ADD_TO_WISHLIST, ADD_TO_FAVOURITE, ADD_TO_STARTED, ADD_TO_COMPLETED, IsGuest, IS_GUEST } from './auth.actions'
import { User } from './user.model';
import { Movie } from '../dashboard/models/movie.interface';



export interface AuthState {
    isLoggedIn: boolean;
    user: User;
    spotlight: Movie;
    isGuest: boolean;

}
const initialAuthState: AuthState = {
    isLoggedIn: false,
    user: undefined,
    spotlight: undefined,
    isGuest: false
}


export function authReducer(state = initialAuthState, action: AuthActions) {


    switch (action.type) {
        case LOGIN_USER:
            return {
                isLoggedIn: true,
                user: action.payload.user,
                spotlight: state.spotlight,
                isGuest: false
            };
            case LOGOUT_USER:
                return {
                    isLoggedIn: false,
                    user: undefined,
                    spotlight: undefined,
                    isGuest: false
                };
            case IS_GUEST:
                return {
                    isLoggedIn: true,
                    user: state.user,
                    spotlight: state.spotlight,
                    isGuest: true
                };

        case SET_SPOTLIGHT:
            return {
                isLoggedIn: state.isLoggedIn,
                user: state.user,
                spotlight: action.payload.movie,
                isGuest: state.isGuest
            }
        case ADD_TO_WISHLIST:
            let updatedUserWishlist = {
                name: state.user.name,
                email: state.user.email,
                favs: state.user.favs,
                startedMovies: state.user.startedMovies,
                completedMovies: state.user.completedMovies,
                wishList: !state.user.wishList.includes(action.payload.movieId) ? state.user.wishList.concat(action.payload.movieId) : state.user.wishList,
                focussed: state.user.focussed
            }
            return {
                isLoggedIn: state.isLoggedIn,
                user: updatedUserWishlist,
                spotlight: state.spotlight,
                isGuest: state.isGuest
            }

        case ADD_TO_FAVOURITE:
            let updatedUserFavourite = {
                name: state.user.name,
                email: state.user.email,
                favs: !state.user.favs.includes(action.payload.movieId) ? state.user.favs.concat(action.payload.movieId) : state.user.favs,
                startedMovies: state.user.startedMovies,
                completedMovies: state.user.completedMovies,
                wishList: state.user.wishList,
                focussed: state.user.focussed
            }
            return {
                isLoggedIn: state.isLoggedIn,
                user: updatedUserFavourite,
                spotlight: state.spotlight,
                isGuest: state.isGuest
            }

        case ADD_TO_STARTED:
            let updatedUserStarted = {
                name: state.user.name,
                email: state.user.email,
                favs: state.user.favs,
                startedMovies: !state.user.startedMovies.includes(action.payload.movieId) ? state.user.startedMovies.concat(action.payload.movieId) : state.user.startedMovies,
                completedMovies: state.user.completedMovies,
                wishList: state.user.wishList,
                focussed: state.user.focussed
            }
            return {
                isLoggedIn: state.isLoggedIn,
                user: updatedUserStarted,
                spotlight: state.spotlight,
                isGuest: state.isGuest
            }
        case ADD_TO_COMPLETED:
            let updatedUserCompleted = {
                name: state.user.name,
                email: state.user.email,
                favs: state.user.favs,
                startedMovies: state.user.startedMovies,
                completedMovies: !state.user.completedMovies.includes(action.payload.movieId) ? state.user.completedMovies.concat(action.payload.movieId) : state.user.completedMovies,
                wishList: state.user.wishList,
                focussed: state.user.focussed
            }
            return {
                isLoggedIn: state.isLoggedIn,
                user: updatedUserCompleted,
                spotlight: state.spotlight,
                isGuest: state.isGuest
            }
        default:
            return state;
    }
}

export const getUser = (state: AuthState) => state.user;
export const getIsLoggedIn = (state: AuthState) => state.isLoggedIn;
export const getSpotlight = (state: AuthState) => state.spotlight;
export const getIsGuest = (state: AuthState) => state.isGuest;