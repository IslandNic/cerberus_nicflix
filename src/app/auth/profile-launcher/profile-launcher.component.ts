
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, noop, Observable} from 'rxjs'
import { tap, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';
import { AuthService } from '../auth.service';
import { User } from '../user.model';
import * as Auth from '../../auth/auth.actions';



@Component({
    selector: 'app-profile-launcher',
    templateUrl: './profile-launcher.component.html',
    styleUrls: ['./profile-launcher.component.scss']
})
export class ProfileLauncherComponent implements OnInit {
    registerForm: FormGroup;
    subscription: Subscription;
    isLoggedIn$: Observable<boolean>;
    submitted = false;
    isLoading$: Observable<boolean>;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private store: Store<fromRoot.State>
    ) { }

    ngOnInit() {
        this.buildForm(); 
        this.isLoading$ = this.store.select(fromRoot.getIsLoading);
        this.isLoggedIn$ = this.store.select(fromRoot.getIsLoggedin);
    }

    get f() { return this.registerForm.controls; }

    buildForm() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    async onSubmit() {
        this.submitted = true;
        if (this.registerForm.valid) {    
            let response = await this.authService.loginUser(this.registerForm.controls['firstName'].value);
            //Todo temp alert, redo in modal if time
            if(!response) alert('Login failed. Try watching as a guest or use the following credentials => First Name: Nicholis and Password: 123456')
            
            
        }
    }

    continue(){
        this.router.navigateByUrl('/dashboard');
    }

    logOut(){
        this.store.dispatch(new Auth.LogoutUser());
    }

    guestLogin(){
        this.authService.loginUser("guest");
    }
}
